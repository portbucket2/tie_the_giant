﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Giant_col_check : MonoBehaviour
{
    private Giant giant;

    public int colCount = 0;


    private void Start()
    {
        giant = transform.parent.GetComponent<Giant>();
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Tail"))
        {
            //giant.totalColCount++;
            //Debug.Log(giant.totalColCount);
            colCount++;

            giant.beingWrapped = true;
        }

        if (other.CompareTag("Player"))
        {
            giant.beingWrapped = true;
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.CompareTag("Tail"))
        {
            //giant.totalColCount--;
            colCount--;
        }
    }
}
