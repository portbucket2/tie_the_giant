﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Food : MonoBehaviour
{
    public GameObject[] simpleVisual;
    public GameObject[] enhancedVisual;

    public bool isExtraFood = false;

    public event Action<int> OnLastFoodEaten;

    public bool randomColor;
    public Particles1 particles;

    public bool init = false;

    public GameObject[] toDisable;
    public GameObject[] toEnable;

    public GameObject candies;
    public GameObject sphere;

    private Vector3 randRot;

    public GameObject text;
    public Text t;

    public MeshFilter mf;

    public float magic;

    public HeadNumber[] headNumber;
    public int initNumber;

    private GameManager gameManager;

    public Food nextFood;

    private Animator animator;

    private int lastRandomChild = 0;

    private void Awake()
    {
        animator = GetComponent<Animator>();
        headNumber[0].SetNumber(initNumber);
        headNumber[1].SetNumber(initNumber);

        gameManager = FindObjectOfType<GameManager>();

        if (!isExtraFood)
        {
            gameManager.totalNumber += initNumber;
            gameManager.firstExtraFood = this;
        }


        if (!init)
        {
            gameObject.SetActive(false);
        }
        else
        {
            gameManager.initFood = this;
        }

        if (Stats.visualType == 0)
        {
            foreach (var item in simpleVisual)
            {
                item.SetActive(true);
            }
            foreach (var item in enhancedVisual)
            {
                item.SetActive(false);
            }
        }
        if (Stats.visualType == 1)
        {
            foreach (var item in simpleVisual)
            {
                item.SetActive(false);
            }
            foreach (var item in enhancedVisual)
            {
                item.SetActive(true);
            }
        }
    }


    public void Activate(int number)
    {

        GetComponent<SphereCollider>().enabled = true;

        foreach (var item in toDisable)
        {
            item.SetActive(true);
        }

        foreach (var item in toEnable)
        {
            item.SetActive(false);
        }

        if (Stats.visualType == 0)
        {
            foreach (var item in simpleVisual)
            {
                item.SetActive(true);
            }
            foreach (var item in enhancedVisual)
            {
                item.SetActive(false);
            }
        }
        if (Stats.visualType == 1)
        {
            foreach (var item in simpleVisual)
            {
                item.SetActive(false);
            }
            foreach (var item in enhancedVisual)
            {
                item.SetActive(true);
            }
        }

        initNumber = number;
        headNumber[0].SetNumber(number);
        headNumber[1].SetNumber(number);

        Start();

        particles.ReActivate();
    }

    private void OnEnable()
    {
        animator.Play("FoodPopIn", 0, 0f);
    }

    private void Start()
    {
        sphere.SetActive(false);

        candies.transform.GetChild(lastRandomChild).gameObject.SetActive(false);

        lastRandomChild = UnityEngine.Random.Range(0, candies.transform.childCount);

        GameObject randomChild = candies.transform.GetChild(lastRandomChild).gameObject;

        randomChild.SetActive(true);

        if (!randomColor)
        {
            particles.SetMaterial(randomChild.GetComponent<MeshRenderer>().material);
        }

        //Relocate();

        randRot.x = UnityEngine.Random.Range(0, 2f);
        randRot.y = UnityEngine.Random.Range(0, 2f);
        randRot.z = UnityEngine.Random.Range(0, 2f);
    }

    private void LateUpdate()
    {
        //float scale = Mathf.Sin(Time.time * 1.5f) + 1f;

        //candies.transform.localScale = Vector3.one * (scale * 2f);

        candies.transform.Rotate(Vector3.right, randRot.x);
        candies.transform.Rotate(Vector3.up, randRot.y);
        candies.transform.Rotate(Vector3.forward, randRot.z);

        //text.transform.position = Camera.main.WorldToScreenPoint(this.transform.position);

        //Vector3 newPos = camFace.transform.position;

        //Vector3 p = Camera.main.transform.position;
        //p.y = transform.position.y;

        //float y = (p - transform.position).magnitude;

        //newPos.y = y * magic * 1.252f + 2f;
        //newPos.y = 1.27f + 1.252f * (y * 0.1f - 3);

        //Vector3 world_v = transform.localToWorldMatrix.MultiplyPoint3x4(mf.mesh.vertices[0]);
        //Vector3 world_pt = transform.TransformPoint(mf.mesh.vertices[0]);

        //text.transform.position = world_pt;

        //camFace.transform.position = newPos;

        //t.text = y.ToString("#");

    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            gameManager.Eaten();
            //Relocate();
            //gameObject.SetActive(false);

            GetComponent<SphereCollider>().enabled = false;

            foreach (var item in toDisable)
            {
                item.SetActive(false);
            }

            foreach (var item in toEnable)
            {
                item.SetActive(true);
            }

            if (nextFood != null)
            {
                nextFood.gameObject.SetActive(true);
            }
            else
            {
                //gameManager.player.arrowTarget = gameManager.giant.gameObject;
                OnLastFoodEaten?.Invoke(initNumber);
            }
        }
    }

    private void Relocate()
    {
        Vector3 newPosition = new Vector3(UnityEngine.Random.Range(-25f, 25f), UnityEngine.Random.Range(3f, 10f), UnityEngine.Random.Range(-25f, 25f));

        transform.position = newPosition;
    }
}
