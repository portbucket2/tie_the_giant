﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HeadNumber : MonoBehaviour
{
    public GameObject onesRoot;
    public GameObject tensRoot;
    public GameObject[] ones;
    public GameObject[] tens;

    private int lastOnes = 0;
    private int lastTens = 0;

    private int number;
    
    private void LateUpdate()
    {
        transform.forward = transform.position - Camera.main.transform.position;
        //transform.LookAt(Camera.main.transform);
    }

    public int GetNumber()
    {
        return number;
    }

    public void SetNumber(int num)
    {
        number = num;

        //for (int i = 0; i < onesRoot.transform.childCount; i++)
        //{
        //    onesRoot.transform.GetChild(i).gameObject.SetActive(false);
        //    tensRoot.transform.GetChild(i).gameObject.SetActive(false);
        //}

        //onesRoot.transform.GetChild(lastOnes).gameObject.SetActive(false);
        //tensRoot.transform.GetChild(lastTens).gameObject.SetActive(false);
        ones[lastOnes].gameObject.SetActive(false);
        tens[lastTens].gameObject.SetActive(false);


        onesRoot.transform.localPosition = new Vector3(37f, 37.9f, 0f);
        
        if (num < 10)
        {
            lastOnes = num;
            onesRoot.transform.localPosition += Vector3.left * 37f;
            //onesRoot.transform.GetChild(num).gameObject.SetActive(true);
            ones[num].gameObject.SetActive(true);
        }
        else
        {
            lastOnes = num - (num / 10) * 10;
            lastTens = num / 10;
            //onesRoot.transform.GetChild(lastOnes).gameObject.SetActive(true);
            //tensRoot.transform.GetChild(lastTens).gameObject.SetActive(true);
            ones[lastOnes].gameObject.SetActive(true);
            tens[lastTens].gameObject.SetActive(true);
        }
    }
}
