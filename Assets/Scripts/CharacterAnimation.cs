﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CharacterAnimation : MonoBehaviour
{

    public event Action<bool> OnWeaponToggle;

    public void ActivateWeapon()
    {
        OnWeaponToggle?.Invoke(true);
    }

    public void DeactivateWeapon()
    {
        OnWeaponToggle?.Invoke(false);
    }
}
