﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CutterRoot : MonoBehaviour
{
    public GameObject cutter;
    private Rigidbody cutterRb;

    public Vector3 rotationSpeed;
    public float acceleration;
    public int direction = 1;
    public int active;
    private float speedMultiplier = 1f;

    public bool playerNear = false;

    private void Start()
    {
        cutterRb = cutter.GetComponent<Rigidbody>();
    }

    private void FixedUpdate()
    {
        speedMultiplier = Mathf.Lerp(speedMultiplier, active, Time.fixedDeltaTime * acceleration);

        //propeller.transform.Rotate(Vector3.back, speedMultiplier * rotationSpeed * Time.fixedDeltaTime);

        cutterRb.MoveRotation(cutterRb.rotation * Quaternion.Euler(direction * speedMultiplier * rotationSpeed * Time.fixedDeltaTime));
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            playerNear = true;
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            playerNear = false;
        }
    }

}
