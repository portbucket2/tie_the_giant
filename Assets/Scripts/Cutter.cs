﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Cutter : MonoBehaviour
{
    public CutterRoot cutterRoot;

    public float reEnableDelay;

    public bool active = true;
    public bool isWeapon = false;

    private GameManager gameManager;

    private void Start()
    {
        gameManager = FindObjectOfType<GameManager>();
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (!active || !cutterRoot.playerNear)
        {
            if (collision.gameObject.CompareTag("Tail"))
            {
                Debug.Log("Tail Lagse!");
            }
            return;
        }

        if (collision.gameObject.CompareTag("Tail"))
        {
            Tail tail = collision.gameObject.GetComponent<Tail>();

            if (tail.isLoose)
            {
                return;
            }
            
            active = false;

            PlayerController.VanishLooseTails(tail.tailNumber, PlayerController.tailCount);

            PlayerController.tailCount = tail.tailNumber - 1;

            PlayerController.TheLastTail = tail.joint.connectedBody;

            PlayerController.TheLastTail.GetComponent<Tail>().SetAsBody(false);

            tail.joint.connectedBody = null;

            //cutterRoot.active = 0;

            gameManager.player.HeadProgressUpdate();

            gameManager.TailCut();
        }
    }

    private void OnCollisionExit(Collision collision)
    {
        if (!active && !isWeapon)
        {
            Invoke("ReEnable", reEnableDelay);
        }
    }

    void ReEnable()
    {
        active = true;

        cutterRoot.active = 1;
    }
}
