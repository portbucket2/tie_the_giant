﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Particles1 : MonoBehaviour
{
    public Rigidbody[] rbs;

    public float aliveDuration;
    public float sizeReduction;

    public void SetDrag(float value)
    {
        foreach (var item in rbs)
        {
            item.drag = value;
        }
    }

    private void Awake()
    {
        foreach (var item in rbs)
        {
            float x = 0.5f;
            //item.AddExplosionForce(100f, Vector3.down, 50f, 50f, ForceMode.Impulse);
            item.transform.localPosition = Vector3.up * Random.Range(-x, x) + Vector3.right * Random.Range(-x, x) + Vector3.forward * Random.Range(-x, x);

            item.transform.Rotate(Vector3.up, Random.Range(-x, x));
            item.transform.Rotate(Vector3.right, Random.Range(-x, x));
            item.transform.Rotate(Vector3.forward, Random.Range(-x, x));

            item.transform.localScale = item.transform.localScale * Random.Range(0.25f, 1f);
        }
    }

    public void ReActivate()
    {
        Awake();
    }

    public void SetMaterial(Material m)
    {
        foreach (var item in rbs)
        {
            item.gameObject.GetComponent<MeshRenderer>().material = m;
        }
    }

    private void FixedUpdate()
    {
        foreach (var item in rbs)
        {
            item.transform.localScale -= item.transform.localScale * Time.fixedDeltaTime * sizeReduction;
        }
    }
    private void Start()
    {
        Invoke("Die", aliveDuration);
    }

    void Die()
    {
        gameObject.SetActive(false);
    }
}
