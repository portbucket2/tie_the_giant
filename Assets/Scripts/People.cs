﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class People : MonoBehaviour
{
    public BallThrower ballThrower;

    public bool willRun = false;

    public float runSpeed;

    private float randomDelay = 0f;

    private GameManager gameManager;

    private Animator animator;

    private int currentState = 0; //0 idle, 1 throw stuff, 2 run scared

    private Vector3 randRot;

    private void Start()
    {
        animator = GetComponent<Animator>();

        gameManager = FindObjectOfType<GameManager>();

        gameManager.OnPeopleStateChange += StateChange;

        randRot.x = Random.Range(0, 2f) + 0.5f;
        randRot.y = Random.Range(0, 2f) + 0.5f;
        randRot.z = Random.Range(0, 2f) + 0.5f;

    }

    public void StateChange(int state)
    {
        if (currentState == -1)
        {
            return;
        }

        currentState = state;

        if (state == 1)
        {
            Invoke("Idle", Random.Range(0.5f, 1.5f));
        }
        else
        {
            animator.SetInteger("State", state);
        }


        if (currentState == 2 && willRun)
        {
            Vector3 newPos = transform.position;
            newPos.y = 0;
            transform.position = newPos;
        }
    }

    void Idle()
    {
        if (currentState == 2)
        {
            return;
        }
        StateChange(0);
        Invoke("Throw", Random.Range(0.5f, 1.5f));
    }
    void Throw()
    {
        if (currentState == 2)
        {
            return;
        }

        animator.SetInteger("State", 1);

        StateChange(1);
    }

    private void FixedUpdate()
    {
        if (currentState == 1)
        {
            Vector3 newPos = gameManager.player.transform.position;

            newPos.y = transform.position.y;

            transform.LookAt(newPos, Vector3.up);
        }

        if (currentState == 2)
        {
            if (!willRun)
            {
                FlyAway();
                return;
            }
            randomDelay -= Time.fixedDeltaTime;

            if (randomDelay <= 0)
            {
                RandomizeDirection();
            }

            transform.position += transform.forward * Time.fixedDeltaTime * runSpeed;
        }

        if (currentState == -1)
        {
            //transform.Rotate(Vector3.right, randRot.x);
            //transform.Rotate(Vector3.up, randRot.y);
            //transform.Rotate(Vector3.forward, randRot.z);
        }

    }

    void RandomizeDirection()
    {
        transform.Rotate(Vector3.up, Random.Range(-120f, 120f));

        randomDelay = Random.Range(0.5f, 5f);
    }


    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Player") || other.CompareTag("Tail"))
        {
            FlyAway();
        }
    }

    void FlyAway()
    {
        if (currentState == -1)
        {
            return;
        }
        StateChange(-1);

        Rigidbody rb = GetComponent<Rigidbody>();
        rb.useGravity = true;

        GetComponent<SphereCollider>().isTrigger = false;

        //float x = 10f;

        //rb.AddForce(Vector3.right * Random.Range(-x, x) + Vector3.forward * Random.Range(-x, x));
        rb.AddExplosionForce(500f, Vector3.down, 30f, 30f, ForceMode.Impulse);

        //rb.AddTorque(Vector3.up * Random.Range(-x, x) + Vector3.right * Random.Range(-x, x) + Vector3.forward * Random.Range(-x, x));

        Invoke("Die", 5f);
    }

    void Die()
    {
        gameObject.SetActive(false);
    }

    public void ThrowBall()
    {
        ballThrower.ThrowBall();
    }
}
