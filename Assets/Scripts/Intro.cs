﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;


public class Intro : MonoBehaviour
{
    public Dropdown visualType;
    public Dropdown winType;
    public Dropdown controlType;
    public Dropdown levelToLoad;

    public GameObject[] debugStuff;

    private void Awake()
    {
        if (!Stats.production)
        {
            foreach (var item in debugStuff)
            {
                item.SetActive(true);
            }
        }
    }

    private void Start()
    {
        visualType.value = Stats.visualType;

        Stats.current_fake_level = PlayerPrefs.GetInt("FakeLevel", 1);

        if (Stats.production)
        {
            Invoke("StartButtonPressed", 2f);
        }
    }

    public void StartButtonPressed()
    {
        Stats.visualType = visualType.value;
        Stats.winType = winType.value;
        Stats.controlType = controlType.value;

        PlayerController.tailCount = 1;
        PlayerController.tails.Clear();

        Stats.cf_multiplier = 1f;

        int idx = Stats.current_fake_level % (SceneManager.sceneCountInBuildSettings - 1);

        if (idx == 0)
        {
            idx = SceneManager.sceneCountInBuildSettings - 1;
        }

        SceneManager.LoadScene(idx);
    }

    private void OnApplicationQuit()
    {
        Shader.DisableKeyword("ENABLE_WORLDBEND");
    }

    public void ClearSave()
    {
        PlayerPrefs.SetInt("FakeLevel", 1);
        Stats.current_fake_level = 1;
    }

    public void LoadLevel()
    {
        ClearSave();
        Stats.current_fake_level = levelToLoad.value + 1;
        StartButtonPressed();
    }
}
