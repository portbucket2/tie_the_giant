﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Giant : MonoBehaviour
{
    public GameObject col;

    public float particleDrag;

    public bool isTower = true;
    public Cutter weapon;

    public GameObject[] simpleVisual;
    public GameObject[] enhancedVisual;

    public CharacterAnimation character; 
    private Animator characterAnimator;
    private int characterState = 0;
    private float attackDelayInit = 0.65f;
    private float attackDelay = 0f;

    public GameObject upCol;

    public float tapIncrement;
    public float tapAndHoldIncrement;
    public float sliderConstantDecrement;

    public bool beingWrapped = false;
    public bool fullyWrapped = false;
    public float wrapDurationBeforeFail = 2f;

    public Slider slider;

    public Giant_col_check[] col_checks;

    public PlayerController player;

    public int minColNeeded;
    public int totalColCount;

    private bool dead = false;
    private bool failed = false;

    private float wrappingDuration = 0f;
    public float wrappingDurationMax;

    public Rigidbody[] rigidBodies;
    public GameObject[] toDisable;
    public Particles1[] toEnable;

    public HeadNumber headNumber;
    public int number;

    private GameManager gameManager;

    private bool tapAndHold = false;

    private void Awake()
    {
        slider.gameObject.SetActive(false);
        upCol.SetActive(false);
        weapon.isWeapon = true;

        if (!isTower)
        {
            return;
        }

        if (Stats.visualType == 0)
        {
            foreach (var item in simpleVisual)
            {
                item.SetActive(true);
            }
            foreach (var item in enhancedVisual)
            {
                item.SetActive(false);
            }
        }
        if (Stats.visualType == 1)
        {
            foreach (var item in simpleVisual)
            {
                item.SetActive(false);
            }
            foreach (var item in enhancedVisual)
            {
                item.SetActive(true);
            }
        }
    }

    private void Start()
    {
        foreach (var item in toEnable)
        {
            item.SetDrag(particleDrag);
        }

        gameManager = FindObjectOfType<GameManager>();

        headNumber.SetNumber(number);

        gameManager.giantHeadNumber = number;

        minColNeeded = col_checks.Length;

        slider.maxValue = wrappingDurationMax;
        slider.minValue = 0;

        //player.arrowTarget = this.gameObject;

        gameManager.giant = this;

        gameManager.OnTapTapEvent += OnTapTap;
        gameManager.OnTapAndHoldEvent += OnTapAndHold;

        characterAnimator = character.GetComponent<Animator>();

        character.OnWeaponToggle += WeaponToggle; 
    }

    private void Update()
    {
        if (dead)
        {
            return;
        }

        slider.value = Mathf.Lerp(slider.value, 0f, slider.maxValue * sliderConstantDecrement * Time.fixedDeltaTime);

        if (tapAndHold)
        {
            slider.value += Time.deltaTime * tapAndHoldIncrement;

            if (slider.value >= 1)
            {
                Die();

                gameManager.tap.gameObject.SetActive(false);
            }
        }
        //    //slider.value = Mathf.Lerp(slider.value, wrappingDuration, 5 * Time.fixedDeltaTime);

        //    slider.value = wrappingDuration;

        //    if (slider.value > 0)
        //    {
        //        slider.gameObject.SetActive(true);
        //    }
        //    else
        //    {
        //        slider.gameObject.SetActive(false);
        //    }
    }

    private void FixedUpdate()
    {
        if (dead || failed || headNumber.GetNumber() > PlayerController.tailCount || fullyWrapped)
        {
            return;
        }

        totalColCount = 0;

        foreach (Giant_col_check col_check in col_checks)
        {
            if (col_check.colCount > 0)
            {
                totalColCount++;
            }
        }

        if (totalColCount >= minColNeeded)
        {

            slider.gameObject.SetActive(true);

            gameManager.tap.gameObject.SetActive(true);
            gameManager.TapTap();
            gameManager.player.ArrowShow(false);
            gameManager.giantFullyWrapped = true;

            upCol.SetActive(true);

            characterAnimator.SetInteger("State", 2);

            weapon.active = false;


            wrappingDuration += Time.fixedDeltaTime;

            if (wrappingDuration >= wrappingDurationMax)
            {
                fullyWrapped = true;

                //dead = true;
                //Invoke("Die", 0.7f);
            }
        }
        else
        {
            wrappingDuration = 0f;
        }


        //LEVEL FAILED IF FAIL TO WRAP

        //if (beingWrapped)
        //{
        //    wrapDurationBeforeFail -= Time.fixedDeltaTime;

        //    if (wrapDurationBeforeFail <= 0)
        //    {
        //        if (headNumber.GetNumber() > PlayerController.tailCount)
        //        {
        //            failed = true;
        //            gameManager.LevelFailed();
        //        }

        //    }
        //}


        if (characterState == 1)
        {
            //Vector3 lookTarget = gameManager.player.transform.position;
            //lookTarget.y = character.transform.position.y;

            //Transform t = character.transform;

            //t.LookAt(lookTarget);

            //Vector3 newRot = character.transform.eulerAngles;

            //newRot.y = Mathf.LerpAngle(newRot.y, t.eulerAngles.y, )

            //character.transform.rotation = Quaternion.Slerp(character.transform.rotation, t.rotation, Time.fixedDeltaTime * 0.005f);

            //float angle = Mathf.LerpAngle(character.transform.eulerAngles.y, t.eulerAngles.y, Time.fixedDeltaTime * 0.001f);

            //character.transform.eulerAngles = new Vector3(0, angle, 0);

            //attackDelay -= Time.fixedDeltaTime;

            //if (attackDelay <= 0)
            //{
            //    characterAnimator.SetInteger("State", Random.Range(0, 1));
            //    attackDelay = attackDelayInit;
            //}
        }
    }

    public void PlayerNear()
    {
        //characterAnimator.SetInteger("State", 1);
        characterState = 1;
        //attackDelay = attackDelayInit;
        //Invoke("ActivateWeapon", 0.2f);
        //Invoke("Idle", Random.Range(1.75f, 2.5f));
        Attack();
    }

    void WeaponToggle(bool value)
    {
        if (characterState == 2)
        {
            weapon.active = false;
            return;
        }
        weapon.active = value;
        Debug.Log("Weapon active: " + value);
    }

    //void ActivateWeapon()
    //{
    //    if (characterState == 2)
    //    {
    //        weapon.active = false;
    //        return;
    //    }
    //    weapon.active = true;
    //}

    //void DeactivateWeapon()
    //{
    //    weapon.active = false;
    //}

    void Idle()
    {
        if (characterState == 2)
        {
            return;
        }
        characterAnimator.SetInteger("State", 0);
        Invoke("Attack", Random.Range(0.85f, 1.25f));
    }

    void Attack()
    {
        if (characterState == 2)
        {
            return;
        }
        characterAnimator.SetInteger("State", 1);
        //Invoke("Idle", Random.Range(1.75f, 2.5f));
    }

    void OnTapTap()
    {
        if (!fullyWrapped)
        {
            return;
        }

        slider.value += slider.maxValue * tapIncrement;

        if (slider.value >= 1)
        {
            Die();

            gameManager.tap.gameObject.SetActive(false);
        }
    }

    void OnTapAndHold(bool value)
    {
        if (!fullyWrapped)
        {
            return;
        }

        tapAndHold = value;

    }

    void Die()
    {
        character.gameObject.SetActive(false);
        gameManager.PeopleStateChange(2);

        //gameObject.SetActive(false);

        foreach (var item in toDisable)
        {
            item.gameObject.SetActive(false);
        }

        foreach (var item in toEnable)
        {
            item.gameObject.SetActive(true);
        }

        foreach (var item in rigidBodies)
        {
            item.isKinematic = false;
            item.useGravity = true;
        }

        Invoke("ResetCamera", 1.5f);
        //Invoke("LevelComplete", 1f);

        LevelComplete();

        gameManager.player.ArrowShow(false);

        dead = true;
    }

    void LevelComplete()
    {
        gameManager.LevelComplete();
    }

    void ResetCamera()
    {
        col.SetActive(false);
        player.giantNear = false;
    }
}
