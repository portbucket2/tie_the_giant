﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BallThrower : MonoBehaviour
{
    private int currentBall = 0;

    public Rigidbody[] balls;

    private GameObject parent;
    private Vector3 initPos;

    private void Start()
    {
        initPos = transform.localPosition;
        parent = transform.parent.gameObject;
        transform.SetParent(parent.transform.parent);
    }

    public void ThrowBall()
    {
        if (currentBall >= balls.Length)
        {
            currentBall = 0;
        }

        balls[currentBall].transform.position = parent.transform.position + initPos;
        balls[currentBall].gameObject.SetActive(true);
        balls[currentBall].AddForce(parent.transform.forward * 35 + transform.up * 65, ForceMode.Impulse);
        currentBall++;
    }
}
