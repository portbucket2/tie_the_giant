﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Starter : MonoBehaviour
{
    public GameObject toDisable;
    
    private void OnTriggerExit(Collider other)
    {
        toDisable.SetActive(false);
    }
}
