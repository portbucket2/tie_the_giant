﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using com.faithstudio.SDK;

public class GameManager : MonoBehaviour
{
    public GameObject tut;

    public int levelType = 1; // 0 collect balls, 1 collect balls + wrap tower

    public int winType = 1; //0 tap tap, 1 tap and hold

    public Giant giant;

    public GameObject Foods;
    public Food initFood;
    public Food firstExtraFood;
    public ExtraFoodSpawner extraFoodSpawner;

    private int totalFoods;
    private int foodsEaten = 0;

    public int totalNumber;
    public int giantHeadNumber;

    public PlayerController player;

    public event Action<int> OnPeopleStateChange;
    public event Action OnTapTapEvent;
    public event Action<bool> OnTapAndHoldEvent;

    public GameObject levelCompleteUI;
    public GameObject levelStartUI;
    public GameObject levelFailUI;

    public Text levelNumber;
    public Text startText;
    public Text levelHint;

    public String[] hints;

    int currentLevel;
    public bool levelFailed = false;

    public Text tap;

    public bool giantFullyWrapped = false;

    private Animator animator;

    private void Start()
    {
        
        animator = GetComponent<Animator>();

        PlayerPrefs.SetInt("FakeLevel", (int)Stats.current_fake_level);

        winType = Stats.winType;

        totalFoods = Foods.transform.childCount;

        player = FindObjectOfType<PlayerController>();
        extraFoodSpawner = FindObjectOfType<ExtraFoodSpawner>();

        OnPeopleStateChange?.Invoke(0);

        currentLevel = Stats.current_fake_level;

        //levelNumber.text = currentLevel.ToString();
        levelNumber.text = Stats.current_fake_level.ToString();

        //startText.text = "Level " + lvl.ToString();


        if (levelType == 0)
        {
            //levelHint.text = "Collect " + totalFoods.ToString() + " Balls!";
            //levelHint.text = "Gain " + totalNumber.ToString() + " Length!";
            startText.text = "Gain " + totalNumber.ToString() + " Length!";
        }
        else if (levelType == 1)
        {
            //levelHint.text = "Collect " + totalFoods.ToString() + " Balls!";
            //levelHint.text = "Tie the Tower!";
            startText.text = "Tie the Tower!";
        }

        levelStartUI.SetActive(true);

        if (winType == 0)
        {
            tap.text = "Tap & Hold to Destroy!";
        }
        else if (winType == 1)
        {
            tap.text = "Tap\nto destroy!";
        }
    }

    public void PeopleStateChange(int state)
    {
        OnPeopleStateChange?.Invoke(state);
    }

    public void TapTap()
    {
        OnTapTapEvent?.Invoke();
    }
    public void TapAndHold(bool value)
    {
        OnTapAndHoldEvent?.Invoke(value);
    }

    public void StartLevel()
    {
        FacebookAnalyticsManager.Instance.FBALevelStart(currentLevel);
        //AdjustSDKManager.Instance.InvokePreConfigEvent(AdjustEventTrack.test_event_delete);
        AdjustSDKManager.Instance.InvokePreConfigEvent(AdjustEventTrack.LEVEL_STARTED);

        levelStartUI.SetActive(false);
        player.gameStarted = true;
        player.ArrowShow(true);

        if (Stats.current_fake_level == 1)
        {
            tut.SetActive(true);

            Invoke("TutorialOff", 3f);
        }
    }

    void TutorialOff()
    {
        tut.SetActive(false);
    }

    public void Restart()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().name);
        PlayerController.tailCount = 1;
        PlayerController.tails.Clear();

        Stats.cf_multiplier = 1f;
    }

    public void LevelComplete()
    {
        FacebookAnalyticsManager.Instance.FBALevelComplete(currentLevel);
        AdjustSDKManager.Instance.InvokePreConfigEvent(AdjustEventTrack.LEVEL_COMPLETED);

        levelCompleteUI.SetActive(true);
    }

    public void LevelFailed()
    {
        FacebookAnalyticsManager.Instance.FBALevelFailed(currentLevel);
        AdjustSDKManager.Instance.InvokePreConfigEvent(AdjustEventTrack.LEVEL_FAILED);

        levelFailUI.SetActive(true);
        levelFailed = true;

        Stats.cf_multiplier = 0f;
    }


    public void NextLevel()
    {
        PlayerController.tailCount = 1;
        PlayerController.tails.Clear();

        Stats.current_fake_level++;

        int next = SceneManager.GetActiveScene().buildIndex + 1;
        if (next >= SceneManager.sceneCountInBuildSettings)
        {
            next = 1;
        }

        SceneManager.LoadScene(next);
    }

    public void Eaten()
    {
        foodsEaten++;

        //if (levelType == 0)
        //{
        //    if (foodsEaten >= totalFoods)
        //    {
        //        LevelComplete();
        //    }
        //}
    }


    public void TailCut()
    {
        Handheld.Vibrate();
        animator.Play("RedPanelFadeOut", 0, 0f);

        if (foodsEaten >= totalFoods)
        {
            extraFoodSpawner.SpawnExtraFoods(0);
        }
    }

}
