﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class PlayerController : MonoBehaviour
{
    public HeadNumber headNumber;

    public GameObject[] debugStuff;

    public GameObject[] simpleVisual;
    public GameObject[] enhancedVisual;

    public bool gameStarted = false;

    public int controlType = 0; //0 drag, 1 joystick

    public Text plus;
    private Animator plusAnim;

    public Text[] HeadProgressText;
    public Text[] HeadProgressGoalText;
    public Slider simpleProgress;
    public int dragonProgressMax;
    private int dragonProgressCurrent = 0;
    public GameObject dragonProgressHead;
    private float dragonProgressHeadEndPos = 144.32f;
    private float dragonProgressHeadStartPos = -151f;
    public Image dragonProgressFill;


    public GameObject allStats;
    public GameObject[] stats;
    public Text whichStats;
    private int currentStats = 0;

    public Text text;
    
    //PLAYER STATS
    public Text v;
    public Text h;
    public Text f;
    public Text m;

    //Camera STATS
    public Text cf;
    public Text cr;
    public Text cr_n;
    public Text cdi;
    public Text cd_max;
    public Text cd_min;

    public GameObject skyCam;
    
    public float camFollowSpeed;
    public float camFollowSpeedNearEnemy;
    public float camRotationSpeed;
    public float camRotationSpeedNearEnemy;
    public float movementSpeed;
    public float rotationSpeed;
    private float rotationAmount;
    //public Cloth tail;
    private Vector3 velocity = Vector3.zero;
    private Vector3 intendedPosition = Vector3.zero;

    public Vector2 sensitivity;
    public Vector3 maxLinearVelocity;
    public float maxAngularVelocity;
    private Vector2 lastMousePos = Vector2.zero;
    private Vector2 DragDifference = Vector2.zero;

    public Vector3 LevelBorder;
    public Vector3 LevelBorderOffset;

    private bool dragging = false;
    private Vector2 dragbase = Vector2.zero;

    public Rigidbody rb;
 

    public GameObject camroot;
    public GameObject tail;

    public Tail initialTail;

    public GameObject arrow;
    public GameObject arrowTarget;
    public GameObject dragonHead;
    private Vector3 headRotation = Vector3.zero;

    public static Rigidbody TheFirstTail;
    public static Rigidbody TheLastTail;

    public static Dictionary<int, Tail> tails = new Dictionary<int, Tail>();    //ALL THE TAILS
    //public static Dictionary<int, Tail> looseTails = new Dictionary<int, Tail>();   //ALL THE LOOSE TAILS

    public static int tailCount = 1;    //HOW MANY TAILS ARE ATTACHED TO THE BODY
    //public static int tailNumber = 1;   //HOW MANY TAILS CREATED IN TOTAL

    public float cameraMaxFar;
    public float cameraMinFar;
    public float cameraDistanceIncrement;
    private Vector3 intendedCameraPosition;
    private Vector3 intendedCameraPositionOffest;
    private Camera camera;
    Quaternion camGiantTargetRotation = new Quaternion();


    public GameObject joystick;
    //public GameObject outerCircle;
    public GameObject innerCircle;
    public float joystickMaxDistance;
    private const float joyCoefficient = 3 * Mathf.PI / 2;


    public bool giantNear = false;
    private Vector3 giantCenter = Vector3.zero;

    public float magnetism;

    public float time = 0f;

    private GameManager gameManager;

    private void Awake()
    {
        if (!Stats.production)
        {
            foreach (var item in debugStuff)
            {
                item.SetActive(true);
            }
        }
    }
    private void Start()
    {
        headNumber.SetNumber(0);

        controlType = Stats.controlType;

        if (Stats.visualType == 0)
        {
            foreach (var item in simpleVisual)
            {
                item.SetActive(true);
            }
            foreach (var item in enhancedVisual)
            {
                item.SetActive(false);
            }
        }
        if (Stats.visualType == 1)
        {
            foreach (var item in simpleVisual)
            {
                item.SetActive(false);
            }
            foreach (var item in enhancedVisual)
            {
                item.SetActive(true);
            }
        }

        plusAnim = plus.GetComponent<Animator>();

        gameManager = FindObjectOfType<GameManager>();

        stats[0].SetActive(true);

        UpdateStats();

        rb = GetComponent<Rigidbody>();

        //sensitivity.x *= Screen.currentResolution.width / 100f;
        //sensitivity.y *= Screen.currentResolution.height / 100f;

        Shader.EnableKeyword("ENABLE_WORLDBEND");

        intendedPosition = transform.position;

        camera = Camera.main;
        intendedCameraPosition = camera.transform.localPosition;
        intendedCameraPositionOffest = camera.transform.localPosition;

        tails.Add(1, TheFirstTail.GetComponent<Tail>());

        dragonProgressMax = gameManager.totalNumber;

        arrowTarget = gameManager.initFood.gameObject;

        simpleProgress.maxValue = gameManager.totalNumber;

        HeadProgressGoalText[0].text = gameManager.totalNumber.ToString();
        HeadProgressGoalText[1].text = gameManager.totalNumber.ToString();
    }

    private void OnApplicationQuit()
    {
        //Shader.DisableKeyword("ENABLE_WORLDBEND");
    }
    private void Update()
    {
        if (!gameStarted)
        {
            return;
        }

        if (Input.GetKeyDown(KeyCode.Escape))
        {

            // Quit the application

            if (!Stats.production)
            {
                SceneManager.LoadScene(0);
            }
            else
            {

                Application.Quit();
            }
        }

        simpleProgress.value = Mathf.Lerp(simpleProgress.value, tailCount - 1, Time.deltaTime * 3f);

        dragonProgressFill.fillAmount = Mathf.Lerp(dragonProgressFill.fillAmount, (float)(tailCount - 1) / dragonProgressMax, Time.deltaTime * 3f);

        Vector2 newHeadPos = Vector2.right * (dragonProgressHeadStartPos + dragonProgressFill.fillAmount * (dragonProgressHeadEndPos - dragonProgressHeadStartPos));

        dragonProgressHead.transform.localPosition = newHeadPos;

        //velocity.z = Input.GetAxis("Vertical");
        //velocity.x = Input.GetAxis("Horizontal");

        time += Time.deltaTime;


        if (Input.GetKeyDown(KeyCode.A))
        {
            AddTail();
        }

        if (Input.GetMouseButtonDown(0))
        {
            if (!dragging)
            {
                dragging = true;
                dragbase = Input.mousePosition;

                joystick.SetActive(controlType == 1);
                joystick.transform.position = Input.mousePosition;
            }
        }

        if (dragging)
        {
            //if (gameManager.levelFailed)
            if (giantNear && tailCount >= gameManager.giantHeadNumber)
            {
                //DragDifference = Vector2.zero;
                //dragging = false;

                innerCircle.transform.localPosition = Vector2.zero;
                joystick.SetActive(false);

                //return;

                if (gameManager.winType == 0)
                {
                    gameManager.TapAndHold(true);
                }
            }

            if (controlType == 1)
            {
                DragDifference = (Vector2)Input.mousePosition - dragbase;
            }
            else if (controlType == 0)
            {
                DragDifference = ((Vector2)Input.mousePosition - lastMousePos) * (Vector2.up * 2960f + Vector2.right * 1440f) * 5f;
            }

            
            //JOYSTICK CONTROLS

            float angle = Mathf.Atan2(-DragDifference.x, -DragDifference.y);

            if (Vector2.Distance(dragbase, Input.mousePosition) < joystickMaxDistance)
            {
                innerCircle.transform.position = Input.mousePosition;
            }
            else
            {
                Vector2 limit = new Vector2(joystickMaxDistance * Mathf.Cos(joyCoefficient - angle), joystickMaxDistance * Mathf.Sin(joyCoefficient - angle));

                innerCircle.transform.localPosition = limit;
            }



            //if (difference.x < 0)
            //{
            //    rotationAmount = difference.x;
            //}
            //else if (difference.x > 0)
            //{
            //    rotationAmount = difference.x;
            //}

            //if (transform.position.y >= 0)
            //{
            //    velocity.y = difference.y * 0.005f;
            //}
            //else
            //{
            //    transform.position = new Vector3(transform.position.x, 0f, transform.position.z);
            //}


            //if (difference.y > 0)
            //{
            //    velocity.y = difference.y;
            //}
            //else if (difference.y < 0)
            //{
            //    if (transform.position.y > 0)
            //    {
            //        velocity.y = difference.y;
            //    }
            //}


        }

        if (Input.GetMouseButtonUp(0))
        {
            if (giantNear && tailCount >= gameManager.giantHeadNumber)
            {
                if (gameManager.winType == 1)
                {
                    gameManager.TapTap();
                }
                else if (gameManager.winType == 0)
                {
                    gameManager.TapAndHold(false);
                }
            }


            DragDifference = Vector2.zero;

            dragging = false;

            innerCircle.transform.localPosition = Vector2.zero;
            joystick.SetActive(false);
        }

        //if (Input.GetKey(KeyCode.A))
        //{
        //    rotationAmount = -1f;
        //}
        //else if (Input.GetKey(KeyCode.D))
        //{
        //    rotationAmount = 1f;
        //}

        //if (Input.GetKey(KeyCode.Space))
        //{
        //    velocity.y = 1f;
        //}
        //else if (Input.GetKey(KeyCode.LeftControl))
        //{
        //    if (transform.position.y > 0)
        //    {
        //        velocity.y = -1f;
        //    }
        //}

        lastMousePos = Vector2.Lerp(lastMousePos, Input.mousePosition, Time.deltaTime * 100f);


        if (Input.GetKey(KeyCode.R))
        {
            Restart();
        }

        //text.text = "Tail Count: " + tailCount.ToString() + "\nFPS: " + (1 / Time.deltaTime).ToString("#");
        text.text = "Time: " + time.ToString("F2");
    }

    private void FixedUpdate()
    {
        if (!gameStarted)
        {
            return;
        }

        //intendedPosition += transform.forward * movementSpeed * Time.deltaTime;

        //intendedPosition += velocity * 0.5f * movementSpeed * Time.deltaTime;

        //rb.MovePosition(intendedPosition);



        //transform.position += transform.forward * movementSpeed * Time.deltaTime;
        //transform.position += velocity * movementSpeed * Time.deltaTime;



        //intendedPosition.x = Mathf.Clamp(intendedPosition.x, -LevelBorder.x, LevelBorder.x);
        //intendedPosition.y = Mathf.Clamp(intendedPosition.y, 1f, LevelBorder.y);
        //intendedPosition.z = Mathf.Clamp(intendedPosition.z, -LevelBorder.z, LevelBorder.z);

        //transform.position = intendedPosition;


        headRotation.y = dragonHead.transform.localRotation.eulerAngles.y;
        headRotation.z = dragonHead.transform.localRotation.eulerAngles.z;

        if (dragging)
        {
            if (!(giantNear && tailCount >= gameManager.giantHeadNumber))
            {
                rotationAmount = DragDifference.x * 0.005f;
            }

            if (controlType == 0)
            {
                velocity.y = Mathf.Lerp(velocity.y, DragDifference.y * 0.0005f, Time.fixedDeltaTime * 2f);
            }
            else
            {
                velocity.y = DragDifference.y * 0.005f;
            }

            //headRotation.x = Mathf.Lerp(headRotation.x, -Mathf.Min(50, rb.velocity.y * 10), 8 * Time.fixedDeltaTime);
        }
        else
        {
            //headRotation.x = Mathf.Lerp(headRotation.x, 0, 4 * Time.fixedDeltaTime);
        }


        velocity.y = Mathf.Clamp(velocity.y, -maxLinearVelocity.y, maxLinearVelocity.y);
        rotationAmount = Mathf.Clamp(rotationAmount, -maxAngularVelocity, maxAngularVelocity);

        rb.velocity = (transform.forward + velocity) * (movementSpeed + tailCount * 0f);

        headRotation.x = Mathf.LerpAngle(headRotation.x, Vector3.SignedAngle(transform.forward, rb.velocity.normalized, transform.right), movementSpeed * Time.fixedDeltaTime);

        dragonHead.transform.localRotation = Quaternion.Euler(headRotation);


        if (giantNear && tailCount >= gameManager.giantHeadNumber)
        {
            Vector3 toGiant = giantCenter - transform.position;
            toGiant.y = giantCenter.y;

            camGiantTargetRotation.SetLookRotation(toGiant, Vector3.up);
            
            camroot.transform.position = Vector3.Lerp(camroot.transform.position, transform.position, Stats.cf_multiplier * camFollowSpeedNearEnemy * toGiant.magnitude / 50f * Time.fixedDeltaTime);

            camroot.transform.rotation = Quaternion.Slerp(camroot.transform.rotation, camGiantTargetRotation, Stats.cf_multiplier * camRotationSpeedNearEnemy * toGiant.sqrMagnitude / 2000f * Time.fixedDeltaTime);

            //camroot.transform.rotation = Quaternion.Slerp(camroot.transform.rotation, transform.rotation, camFollowSpeed * Time.fixedDeltaTime);



            //AUTOMATICALLY WRAP THE GIANT

            Vector3 giantCenterSameY = giantCenter;
            giantCenterSameY.y = transform.position.y;



            float angleToGiant = Vector3.SignedAngle(toGiant, transform.forward, Vector3.up);

            float newRotation = -angleToGiant * magnetism / 1000f / toGiant.magnitude;

            //text.text = angleToGiant.ToString();
            //text.text = toGiant.magnitude.ToString();

            if (Mathf.Abs(angleToGiant) > 80 && Mathf.Abs(angleToGiant) < 100)
            {
                transform.Rotate(Vector3.up, (newRotation + rotationAmount) * rotationSpeed * Time.deltaTime);

            }
            else
            {
                transform.Rotate(Vector3.up, rotationAmount * rotationSpeed * Time.deltaTime);
            }



            //Vector2 toGiantXZ = new Vector2(toGiant.x, toGiant.z);
            //Vector2 forwardXZ = new Vector2(transform.forward.x, transform.forward.z);

            //float angleToGiant = Vector2.Angle(forwardXZ, toGiantXZ);


            //if (Mathf.Abs(angleToGiant) < 45)
            //{
            //    float newRotationAmount = (1 - angleToGiant / 45f) * 2f;
            //    transform.Rotate(Vector3.up, Mathf.Sign(angleToGiant) * newRotationAmount * rotationSpeed * Time.deltaTime);
            //}
            //else
            //{
            //    float newRotationAmount = rotationAmount;
            //    transform.Rotate(Vector3.up, newRotationAmount * rotationSpeed * Time.deltaTime);
            //}



            //transform.Rotate(Vector3.up, 3 * rotationAmount * rotationSpeed * Time.deltaTime);
        }
        else
        {
            camroot.transform.position = Vector3.Lerp(camroot.transform.position, transform.position, camFollowSpeed * Time.fixedDeltaTime);

            camroot.transform.rotation = Quaternion.Slerp(camroot.transform.rotation, transform.rotation, camRotationSpeed * Time.fixedDeltaTime);

            transform.Rotate(Vector3.up, rotationAmount * rotationSpeed * Time.deltaTime);
        }



        velocity = Vector3.zero;
        rotationAmount = 0f;
        //intendedCameraPosition = camera.transform.position;
        camera.transform.localPosition = Vector3.Lerp(camera.transform.localPosition, intendedCameraPosition, camFollowSpeed * Time.fixedDeltaTime);


        intendedCameraPosition.z = Mathf.Max(cameraMaxFar, Mathf.Min(-(tailCount*cameraDistanceIncrement), cameraMinFar));

        //intendedCameraPosition.z = Mathf.Clamp(intendedCameraPosition.z, cameraMaxFar, cameraMinFar);

        arrow.transform.LookAt(arrowTarget.transform);

        Vector3 skyCamPos = skyCam.transform.localPosition;
        skyCamPos.y = camroot.transform.localPosition.y * 0.1f;
        skyCam.transform.localPosition = skyCamPos;
    }

    public void HeadProgressUpdate()
    {
        HeadProgressText[0].text = Mathf.Min((tailCount - 1), gameManager.totalNumber).ToString();
        HeadProgressText[1].text = Mathf.Min((tailCount - 1), gameManager.totalNumber).ToString();
        headNumber.SetNumber(Mathf.Min((tailCount - 1), gameManager.totalNumber));
    }

    public void AddTail()
    {

        Vector3 newPosition = TheLastTail.position - TheLastTail.transform.forward * 1f;
        Quaternion newRotation = TheLastTail.rotation;

        GameObject newTail = Instantiate(tail, newPosition, newRotation);

        tails[tailCount].SetAsBody(true);

        tailCount++;
        //tailNumber++;

        HeadProgressUpdate();

        //tails[tailNumber].SetAsBody(false);

        Tail newTailScript = newTail.GetComponent<Tail>();

        newTailScript.tailNumber = tailCount;
        newTailScript.isLoose = false;

        tails.Add(tailCount, newTailScript);

        
        
        //Debug.Log(tails[tailNumber].tailNumber);
    }

    public static void VanishLooseTails(int startingFrom, int endTo)
    {
        int cumulativeDelay = 1;
        //Debug.Log(startingFrom + "  " + endTo);

        for (int i = startingFrom; i <= endTo; i++)
        {
            //looseTails.Add(i, tails[i]);
            tails[i].isLoose = true;
            tails[i].Vanish(cumulativeDelay);
            tails.Remove(i);

            cumulativeDelay++;
        }
    }

    public void Restart()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().name);
        tailCount = 1;
        //tailNumber = 1;
        tails.Clear();
    }

    //public void AddTailFront()
    //{
    //    Vector3 newPosition = TheFirstTail.position + TheFirstTail.transform.forward * 0.85f;
    //    Quaternion newRotation = TheFirstTail.rotation;

    //    GameObject newTail = Instantiate(tail, newPosition, newRotation);

    //    newTail.GetComponent<Tail>().joint.connectedBody = rb;

    //    Rigidbody tailRb = newTail.GetComponent<Rigidbody>();

    //    TheFirstTail.GetComponent<Tail>().joint.connectedBody = tailRb;
    //    TheFirstTail = tailRb;

    //    tailCount++;

    //    intendedCameraPosition.z -= cameraDistanceIncrement;

    //    intendedCameraPosition.z = Mathf.Clamp(intendedCameraPosition.z, cameraMaxFar, cameraMinFar);
    //}

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Food"))
        {
            Food food = other.GetComponent<Food>();

            int num = food.initNumber;


            if (food.nextFood == null)
            {
                if (tailCount - 1 + num < gameManager.totalNumber)
                {
                    arrowTarget = gameManager.firstExtraFood.gameObject;
                }
                else
                {
                    arrowTarget = gameManager.giant.gameObject;
                }
            }
            else
            {
                arrowTarget = food.nextFood.gameObject;
            }


            plus.text = "+" + num.ToString();

            int x = 180;

            plus.transform.localPosition = new Vector2(Random.Range(-x, x), Random.Range(-x, x));

            plusAnim.Play("Plus", 0, 0f);

            dragonProgressCurrent += num;

            float delay = 0f;

            for (int i = 0; i < num; i++)
            {
                Invoke("AddTail", delay);

                delay += 0.15f;
            }

            //Invoke("AddTail", 0f);
            //Invoke("AddTail", 0.15f);
            //Invoke("AddTail", 0.3f);
            //Invoke("AddTail", 0.45f);
            //Invoke("AddTail", 0.60f);
            //Invoke("AddTail", 0.75f);
            //AddTail();
            //AddTail();
            //AddTail();
            //AddTail();
        }

        if (other.CompareTag("Giant Area"))
        {
            giantNear = true;
            giantCenter = other.transform.position;

            gameManager.PeopleStateChange(1);

            //gameManager.giant.PlayerNear();
        }

        if (other.CompareTag("Attack Range"))
        {
            //giantNear = true;
            //giantCenter = other.transform.position;

            gameManager.PeopleStateChange(1);

            gameManager.giant.PlayerNear();
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.CompareTag("Giant Area"))
        {
            giantNear = false;
        }
    }

    public void UpdateStats()
    {
        maxLinearVelocity.y = Stats.v_val;
        rotationSpeed = Stats.h_val;
        movementSpeed = Stats.f_val;
        magnetism = Stats.m_val;

        camFollowSpeed = Stats.cf_val;
        camFollowSpeedNearEnemy = Stats.cf_n_val;
        camRotationSpeed = Stats.cr_val;
        camRotationSpeedNearEnemy = Stats.cr_n_val;
        cameraDistanceIncrement = Stats.cdi_val;
        cameraMaxFar = -Stats.cd_max_val;
        cameraMinFar = -Stats.cd_min_val;


        v.text = "up-down speed\n" + maxLinearVelocity.y.ToString("F2");
        h.text = "right-left rotation\n" + rotationSpeed.ToString("F1");
        f.text = "velocity\n" + movementSpeed.ToString("F1");
        m.text = "magnetic to giant\n" + magnetism.ToString("F1");


        cf.text = "cam follow speed\n" + camFollowSpeed.ToString("F1");
        cr.text = "cam rot speed\n" + camRotationSpeed.ToString("F1");
        cr_n.text = "cam rot near giant\n" + camRotationSpeedNearEnemy.ToString("F1");
        cdi.text = "cam dist increment\n" + cameraDistanceIncrement.ToString("F1");
        cd_max.text = "cam dist max\n" + Stats.cd_max_val.ToString("F1");
        cd_min.text = "cam dist min\n" + Stats.cd_min_val.ToString("F1");
    }

    
    public void NextStats()
    {
        stats[currentStats].SetActive(false);

        currentStats++;
        if (currentStats >= stats.Length)
        {
            currentStats = 0;
        }

        stats[currentStats].SetActive(true);

        whichStats.text = stats[currentStats].name;
    }

    public void PrevStats()
    {
        stats[currentStats].SetActive(false);

        currentStats--;
        if (currentStats < 0)
        {
            currentStats = stats.Length - 1;
        }

        stats[currentStats].SetActive(true);

        whichStats.text = stats[currentStats].name;
    }


    public void ToggleStats()
    {
        allStats.SetActive(!allStats.activeInHierarchy);
    }

    // PLAYER STATS

    public void IncreaseV()
    {
        Stats.v_val += 0.05f;
    }
    public void DecreaseV()
    {
        if (Stats.v_val <= 0)
        {
            Stats.v_val = 0f;
            return;
        }
        Stats.v_val -= 0.05f;
    }
    public void IncreaseH()
    {
        Stats.h_val += 2.5f;
    }
    public void DecreaseH()
    {
        if (Stats.h_val <= 0)
        {
         Stats.h_val = 0f;
            return;
        }
        Stats.h_val -= 2.5f;
    }
    public void IncreaseF()
    {
        Stats.f_val += 0.1f;
    }
    public void DecreaseF()
    {
        if (Stats.f_val <= 0)
        {
            Stats.f_val = 0;
            return;
        }
        Stats.f_val -= 0.1f;
    }
    public void IncreaseM()
    {
        Stats.m_val += 0.5f;
    }
    public void DecreaseM()
    {
        if (Stats.m_val <= 0)
        {
            Stats.m_val = 0f;
            return;
        }
        Stats.m_val -= 0.5f;
    }




    // CAMERA STATS

    public void CF_Increase()
    {
        Stats.cf_val += 0.1f;
    }
    public void CF_Decrease()
    {
        if (Stats.cf_val <= 0)
        {
            Stats.cf_val = 0f;
            return;
        }
        Stats.cf_val -= 0.1f;
    }

    public void CR_Increase()
    {
        Stats.cr_val += 0.1f;
    }

    public void CR_Decrease()
    {
        if (Stats.cr_val <= 0)
        {
            Stats.cr_val = 0f;
            return;
        }
        Stats.cr_val -= 0.1f;
    }

    public void CRN_Increase()
    {
        Stats.cr_n_val += 0.1f;
    }

    public void CRN_Decrease()
    {
        if (Stats.cr_n_val <= 0)
        {
            Stats.cr_n_val = 0f;
            return;
        }
        Stats.cr_n_val -= 0.1f;
    }

    public void CDI_Increase()
    {
        Stats.cdi_val += 0.1f;
    }

    public void CDI_Decrease()
    {
        if (Stats.cdi_val <= 0)
        {
            Stats.cdi_val = 0f;
            return;
        }
        Stats.cdi_val -= 0.1f;
    }

    public void CDMAX_Increase()
    {
        Stats.cd_max_val += 0.1f;
    }

    public void CDMAX_Decrease()
    {
        if (Stats.cd_max_val <= 0)
        {
            Stats.cd_max_val = 0f;
            return;
        }
        Stats.cd_max_val -= 0.1f;
    }

    public void CDMIN_Increase()
    {
        Stats.cd_min_val += 0.1f;
    }

    public void CDMIN_Decrease()
    {
        if (Stats.cd_min_val <= 0)
        {
            Stats.cd_min_val = 0f;
            return;
        }
        Stats.cd_min_val -= 0.1f;
    }


    public void ArrowShow(bool value)
    {
        arrow.gameObject.SetActive(value);
    }

}
