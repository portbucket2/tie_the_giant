﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Stats : MonoBehaviour
{
    public static float v_val = 0.85f;
    public static float h_val = 95f;
    public static float f_val = 12f;
    public static float m_val = 120f;

    public static float cf_multiplier = 1f;
    public static float cf_val = 2.5f;
    public static float cr_val = 4.6f;
    public static float cf_n_val = 7f;
    public static float cr_n_val = 4f;
    public static float cdi_val = 0.5f;
    public static float cd_max_val = 16f;
    public static float cd_min_val = 3f;


    // ZAID BHAI 1
    //public static float v_val = 1.5f;
    //public static float h_val = 95f;
    //public static float f_val = 10f;
    //public static float m_val = 100f;

    //public static float cf_multiplier = 1f;
    //public static float cf_val = 8f;
    //public static float cr_val = 4.5f;
    //public static float cf_n_val = 4.0f;
    //public static float cr_n_val = 4f;
    //public static float cdi_val = 0.5f;
    //public static float cd_max_val = 6.5f;
    //public static float cd_min_val = 0.0f;

    public static int current_fake_level = 1;
    public static int winType = 0; //0 tap and hold, 1 tap tap
    public static int visualType = 0; //0 simple, 1 enhanced
    public static int controlType = 0; //0 drag, 1 joystick
    public static bool production = true;
}