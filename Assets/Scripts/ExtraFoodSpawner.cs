﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ExtraFoodSpawner : MonoBehaviour
{
    public Food LastFood;

    public Food[] ExtraFoods;

    private GameManager gameManager;

    private void Start()
    {
        gameManager = FindObjectOfType<GameManager>();
        gameManager.firstExtraFood = ExtraFoods[0];
        LastFood.OnLastFoodEaten += SpawnExtraFoods;//spawn extra foods if needed after last food eaten
    }


    public void SpawnExtraFoods(int number)
    {
        if (PlayerController.tailCount - 1 + number < gameManager.totalNumber)
        {
            int dividend = gameManager.totalNumber - (PlayerController.tailCount - 1 + number);

            int divisor = ExtraFoods.Length;

            int quotient = Mathf.FloorToInt((float)dividend / divisor);

            int remainder = dividend % divisor;

            //Debug.Log("tailCount: " + PlayerController.tailCount + ", totalNumber: " + gameManager.totalNumber);
            //Debug.Log("dividend: " + dividend + ", divisor: " + divisor + ", quotient: " + quotient + ", remainder: " + remainder);

            //foreach (var item in ExtraFoods)
            //{
            //    item.Activate(quotient);
            //}

            gameManager.player.arrowTarget = ExtraFoods[0].gameObject;
            
            ExtraFoods[0].gameObject.SetActive(true);
            ExtraFoods[0].Activate(quotient);

            for (int i = 1; i < ExtraFoods.Length - 1; i++)
            {
                ExtraFoods[i].gameObject.SetActive(false);
                ExtraFoods[i].Activate(quotient);
            }

            ExtraFoods[ExtraFoods.Length - 1].gameObject.SetActive(false);
            ExtraFoods[ExtraFoods.Length - 1].Activate(quotient + remainder);

        }
        
    }
}
