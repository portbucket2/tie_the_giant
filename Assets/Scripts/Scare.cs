﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Scare : MonoBehaviour
{
    public People[] scareTarget;

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Player") || other.CompareTag("Tail"))
        {
            foreach (var item in scareTarget)
            {
                item.StateChange(2);
            }
        }
    }
}
