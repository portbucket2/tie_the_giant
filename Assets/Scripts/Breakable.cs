﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Breakable : MonoBehaviour
{
    public Rigidbody[] Debris;


    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Player") || other.CompareTag("Tail"))
        {
            foreach (Rigidbody item in Debris)
            {
                item.isKinematic = false;
                item.AddExplosionForce(Random.Range(30f, 55f), -transform.up, 60f, 10);
            }

            GetComponent<SphereCollider>().enabled = false;
        }
    }
}
