﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Tail : MonoBehaviour
{
    public GameObject[] simpleVisual;
    public GameObject[] enhancedVisual;


    public GameObject body;
    public GameObject finalTail;

    public ConfigurableJoint joint;
    public Rigidbody rb;
    public int tailNumber;

    public float initialVanishDelay;
    public float vanishDelay;

    public bool isLoose = false;

    private void Awake()
    {
        if (PlayerController.TheLastTail != null)
        {
            joint.connectedBody = PlayerController.TheLastTail;

            tailNumber = 1;
            isLoose = false;

            SetAsBody(false);
        }
        
        PlayerController.TheLastTail = rb;

        if (PlayerController.TheFirstTail == null)
        {
            PlayerController.TheFirstTail = rb;
        }

        //rb.drag = Mathf.Min(PlayerController.tailCount * 0.75f, 5);

        if (Stats.visualType == 0)
        {
            foreach (var item in simpleVisual)
            {
                item.SetActive(true);
            }
            foreach (var item in enhancedVisual)
            {
                item.SetActive(false);
            }
        }
        if (Stats.visualType == 1)
        {
            foreach (var item in simpleVisual)
            {
                item.SetActive(false);
            }
            foreach (var item in enhancedVisual)
            {
                item.SetActive(true);
            }
        }
    }

    public void SetAsBody(bool value)
    {
        body.SetActive(value);
        finalTail.SetActive(!value);
    }

    public void Vanish(float delayMultiplier)
    {
        Invoke("DelayedVanish", initialVanishDelay + vanishDelay * delayMultiplier);
    }

    void DelayedVanish()
    {
        //yield return new WaitForSeconds(delay);
        gameObject.SetActive(false);
    }
}
